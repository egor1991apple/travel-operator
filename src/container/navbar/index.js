import React from "react"
import Navs from '../../components/nav';
import Modal from "../../components/modal";
import {useStaticQuery, graphql} from "gatsby";
import {MdPeople} from 'react-icons/md';
import {get} from "lodash";


const Navbar = () => {


    const queryData = useStaticQuery(query);
    const navs = get(queryData, 'allRouteJson.nodes[0].routes');


    return (
        <div className={'navbar'}>
            <div className="navbar--container">
                <div className="navbar--logo">
                    Привет мир
                </div>
                <div className="navbar--nav">
                    <Navs data={navs}/>
                    <Modal
                        btnIcon={()=><MdPeople/>}
                        btnText={"Авторизация"}
                        btnStyle={"btn bg-primary-400 hover_bg-primary-500 color-light-50  borderColor-primary-500 mr-10"}>
                        Форма авторизации
                    </Modal>
                </div>
            </div>
        </div>
    )
}


export const query = graphql`
    {
        allRouteJson {
            nodes {
                routes {
                    href
                    title
                    icon
                    childrenLinks {
                        href
                        title
                        icon
                    }
                }
            }
        }
    }
`
export default Navbar
