import React from "react"


import Layout from "../layout/layout"
import SEO from "../layout/seo"

const IndexPage = () => (
  <Layout pageInfo={{ pageName: "index" }}>
    <SEO title="Home" keywords={[`gatsby`, `react`, `bootstrap`]} />

  </Layout>
)

export default IndexPage
