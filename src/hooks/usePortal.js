import {useEffect, useState} from 'react';


export const usePortal = (idElement = null) =>{

    const [node, setNode] = useState(null);

    useEffect(()=>{

       if(typeof document != 'undefined' && document != null){
           const div = document.createElement('div');
           div.id = idElement;
           document.body.appendChild(div);
           setNode(div);
       }
    },[]);


  return node;
}