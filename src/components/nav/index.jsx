import React from 'react';
import {Link} from 'gatsby';
import DropDown from "../dropdown";



function NavItem(props) {

    const  {title = null, href = null, childrenLinks = []} = props;

    return childrenLinks.length === 0
        ? <Link to={href} activeClassName={'active'}>{title}</Link>
        : <DropDown {...props} />

}

function Navs({data = []}) {

    return data.length > 0 &&
        data.map((item = null, index=0) =>
            <NavItem key={`nav--item-${Date.now() * index}`} {...item}></NavItem>
        )
}

export default Navs;