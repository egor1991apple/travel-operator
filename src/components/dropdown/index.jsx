import React, {useRef, useState} from 'react';
import Popper from "../popper";
import {Link} from "gatsby";
import {IoIosArrowDown} from "react-icons/io";
import Fade from "../animation/fade";


const DropDown = ({title = null, href = null, childrenLinks = []}) => {

    const targetRef = useRef(null);

    const [toggle,setToggle] = useState(false);

    return (
        <>
            <Link ref={targetRef} to={href} activeClassName={'active'}>{title}</Link>
            <IoIosArrowDown onClick={()=>setToggle(toggle=> !toggle)}/>

                <Popper targetElement={targetRef.current} >
                    <Fade show={toggle}>
                        {
                            childrenLinks.length > 0 ?
                                childrenLinks.map(({title = null, href = null}, index) =>
                                    <Link key={`dropdown-link_${Date.now()*index}`} to={href} activeClassName={'active'}>{title}</Link>)
                                : null
                        }
                    </Fade>
                </Popper>
        </>

    );
}

export default DropDown;

