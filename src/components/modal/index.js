import React, {useRef, useState} from 'react';
import Fade from "../animation/fade";
import Slide from "../animation/slide";
import usePortal from "react-useportal/dist/usePortal";
import useOnClickOutside from "../../hooks/useOnClickOutside";
function Modal({
                btnStyle='',
                btnIcon= ()=>{},
                btnText= null,
                children = null
               }) {

    const [toggle, setToggle] = useState(false);
    const {Portal} = usePortal()

    const ref = useRef();
    useOnClickOutside(ref, ()=>setToggle(false));

    return (
        <div>
            <button onClick={() => setToggle(!toggle)} className={btnStyle}>{btnIcon()} {btnText} </button>
            <Portal>
                <Fade show={toggle}>
                    <div className="modal"
                         style={{position: 'fixed', background: 'green', height: "100%", width: '100%'}}>
                        <Slide show={toggle}>
                            <div className="modal--content" ref={ref}>
                                {children}
                            </div>
                        </Slide>
                    </div>
                </Fade>
            </Portal>
        </div>


    );
}

export default Modal;