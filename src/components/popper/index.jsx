import React, {useState, useEffect} from 'react';
import {createPortal} from 'react-dom';
import {usePopper} from 'react-popper';
import usePortal from "react-useportal/dist/usePortal";

const Popper = ({children, targetElement = null, namePortal='portal', show = false}) => {

    const { Portal } = usePortal()

    const [popperElement, setPopperElement] = useState(null)

    const {styles, attributes} = usePopper(targetElement, popperElement)

    return <Portal>
                <div ref={setPopperElement} style={styles.popper} {...attributes.popper}>
                        {children}
                </div>
        </Portal>

};

export default Popper;