import React from 'react';
import {useSpring, animated} from 'react-spring';

function Fade({children = null, show = false}) {

    const props = useSpring({
        reverse: !show,
        from: {opacity: 0},
        to: async (next, cancel) => {
            await next({opacity: 1})
        },

    })

    return <animated.div style={props}>{children}</animated.div>
}

export default Fade;