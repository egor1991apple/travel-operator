import React from 'react';
import {useSpring, animated} from 'react-spring';

function Slide({children = null, show = false}) {

    const props = useSpring({
        reverse: !show,
        from: {transform: 'translateY(-1000px)'},
        to: async (next, cancel) => {
            await next({transform: 'translateY(0)'})
        },

    })

    return <animated.div style={props}>{children}</animated.div>
}

export default Slide;