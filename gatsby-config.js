module.exports = {
    pathPrefix: "/travel-operator",
    siteMetadata: {
        title: `Gatsby React Bootstrap Starter`,
        description: `A starter that includes react-bootstrap and react-icons, along with SASS compilation.`,
        author: `Billy Jacoby`,
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        `gatsby-transformer-json`,

        {
            resolve: `gatsby-plugin-portal`,
            options: {
                key: 'portal',
                id: 'portal',
            },
        },


        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `data`,
                path: `${__dirname}/data/`,
                plugins: [
                    `gatsby-transformer-sharp`,
                ]
            },
        },
        `gatsby-plugin-sass`,
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `gatsby-starter-react-bootstrap`,
                short_name: `react-bootstrap`,
                start_url: `/`,
                background_color: `#20232a`,
                theme_color: `#20232a`,
                display: `minimal-ui`,
            },
        },
        // this (optional) plugin enables Progressive Web App + Offline functionality
        // To learn more, visit: https://gatsby.dev/offline
        // `gatsby-plugin-offline`,
    ],
}
